package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/boguswojcik/sad-consumer/mood"
	"github.com/Shopify/sarama"
	"github.com/golang/protobuf/proto"
)

//go:generate protoc -I ./vendor/bitbucket.org/boguswojcik/mood-schema/ ./vendor/bitbucket.org/boguswojcik/mood-schema/mood.proto --go_out=./mood/

func main() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	cfg := sarama.NewConfig()

	consumer, err := sarama.NewConsumer([]string{"localhost:9092"}, cfg)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		if err := consumer.Close(); err != nil {
			log.Fatalln(err)
		}
	}()

	partitionConsumer, err := consumer.ConsumePartition("events", 0, sarama.OffsetNewest)
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := partitionConsumer.Close(); err != nil {
			log.Fatalln(err)
		}
	}()

	fmt.Println("Sad consumer started.")

	for {
		select {
		case <-sigs:
			fmt.Println("Sad consumer exited.")
			os.Exit(0)

		case msg := <-partitionConsumer.Messages():

			event := &mood.Event{}
			proto.Unmarshal(msg.Value, event)

			if event.Type == mood.EventType_SADNESS {
				fmt.Printf("Happy consumer is currently %s. This mood has %d points out of 10.\n", event.MoodName, event.MoodStrenght)
			}

		default:
			time.Sleep(time.Duration(500) * time.Millisecond)
		}
	}
}
